import 'package:flutter/material.dart';

class CustomRegisterButton extends StatefulWidget {
  final bool addMargin;

  const CustomRegisterButton({Key? key, required this.addMargin})
      : super(key: key);

  @override
  State<CustomRegisterButton> createState() => _CustomRegisterButtonState();
}

class _CustomRegisterButtonState extends State<CustomRegisterButton> {
  var isHovered = false;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: () {},
        onHover: (value) {
          setState(() {
            isHovered = value;
          });
        },
        child: Ink(
          child: Container(
            padding: const EdgeInsets.all(12.0),
            margin: widget.addMargin
                ? const EdgeInsets.only(
                    top: 24.0,
                    bottom: 24.0,
                    left: 20.0,
                    right: 20.0,
                  )
                : EdgeInsets.zero,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: isHovered
                    ? [
                        const Color.fromRGBO(44, 122, 123, 1),
                        const Color.fromRGBO(43, 108, 176, 1),
                      ]
                    : [
                        const Color.fromRGBO(49, 151, 149, 1),
                        const Color.fromRGBO(49, 130, 206, 1),
                      ],
              ),
              borderRadius: BorderRadius.circular(12.0),
            ),
            child: const Center(
              child: FittedBox(
                fit: BoxFit.cover,
                child: Text(
                  'Kostenlos Registrieren',
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(230, 255, 250, 1),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
