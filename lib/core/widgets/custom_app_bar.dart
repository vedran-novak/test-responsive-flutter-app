import 'package:flutter/material.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(150.0);
}

class _CustomAppBarState extends State<CustomAppBar> {
  bool _isButtonHovered = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 5.0,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [
                  Color.fromRGBO(49, 151, 149, 1),
                  Color.fromRGBO(49, 130, 206, 1),
                ],
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 67.0,
            decoration: const BoxDecoration(
              color: Color.fromRGBO(255, 255, 255, 1),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(12.0),
                  bottomRight: Radius.circular(12.0)),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.16),
                  blurRadius: 6,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 17.0),
                  child: TextButton(
                    onPressed: () {},
                    onHover: (value) {
                      setState(() => _isButtonHovered = value);
                    },
                    style: ButtonStyle(
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.transparent),
                    ),
                    child: Text(
                      'Login',
                      style: TextStyle(
                        shadows: [
                          Shadow(
                              color: _isButtonHovered
                                  ? const Color.fromRGBO(56, 178, 172, 1)
                                  : const Color.fromRGBO(49, 151, 149, 1),
                              offset: const Offset(0, -4))
                        ],
                        color: Colors.transparent,
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                        letterSpacing: 0.84,
                        decorationColor: _isButtonHovered
                            ? const Color.fromRGBO(56, 178, 172, 1)
                            : const Color.fromRGBO(49, 151, 149, 1),
                        decoration: _isButtonHovered
                            ? TextDecoration.underline
                            : TextDecoration.none,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
