import 'package:flutter/material.dart';
import 'package:test_mobile_index_vedran_novak/screens/desktop/home_desktop_screen.dart';
import 'package:test_mobile_index_vedran_novak/screens/mobile/home_mobile_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 600) {
          return const HomeMobileScreen();
        } else {
          return const HomeDesktopScreen();
        }
      },
    );
  }
}
