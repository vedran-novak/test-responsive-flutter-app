import 'package:flutter/material.dart';
import 'package:test_mobile_index_vedran_novak/screens/mobile/widgets/mobile_custom_segmented_buttons.dart';
import 'package:test_mobile_index_vedran_novak/screens/mobile/widgets/mobile_tab_view.dart';
import 'package:test_mobile_index_vedran_novak/screens/mobile/widgets/sections/mobile_header_section.dart';

import '../../core/widgets/custom_app_bar.dart';
import 'widgets/mobile_custom_bottom_sheet.dart';

class HomeMobileScreen extends StatefulWidget {
  const HomeMobileScreen({super.key});

  @override
  State<HomeMobileScreen> createState() => _HomeMobileScreenState();
}

class _HomeMobileScreenState extends State<HomeMobileScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            const MobileHeader(),
            MobileCustomSegmentedButtons(tabController: _tabController),
            SizedBox(
              height: 1280,
              child: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: const <Widget>[
                  MobileTabView(
                    title: 'Drei einfache Schritte zu deinem neuen Job',
                    firstSectionTitle: 'Erstellen dein Lebenslauf',
                    firstSectionImage:
                        'assets/images/undraw_Profile_data_re_v81r.svg',
                    secondSectionTitle: 'Erstellen dein Lebenslauf',
                    secondSectionImage: 'assets/images/undraw_task_31wc.svg',
                    thirdSectionTitle: 'Mit nur einem Klick bewerben',
                    thirdSectionImage:
                        'assets/images/undraw_personal_file_222m.svg',
                  ),
                  MobileTabView(
                    title: 'Drei einfache Schritte zu deinem neuen Mitarbeiter',
                    firstSectionTitle: 'Erstellen dein Unternehmensprofil',
                    firstSectionImage:
                        'assets/images/undraw_Profile_data_re_v81r.svg',
                    secondSectionTitle: 'Erstellen ein Jobinserat',
                    secondSectionImage:
                        'assets/images/undraw_about_me_wa29.svg',
                    thirdSectionTitle: 'Wähle deinen neuen Mitarbeiter aus',
                    thirdSectionImage:
                        'assets/images/undraw_swipe_profiles1_i6mr.svg',
                  ),
                  MobileTabView(
                    title:
                        'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter',
                    firstSectionTitle: 'Erstellen dein Unternehmensprofil',
                    firstSectionImage:
                        'assets/images/undraw_Profile_data_re_v81r.svg',
                    secondSectionTitle:
                        'Erhalte Vermittlungs- angebot von Arbeitgeber',
                    secondSectionImage:
                        'assets/images/undraw_job_offers_kw5d.svg',
                    thirdSectionTitle:
                        'Vermittlung nach Provision oder Stundenlohn',
                    thirdSectionImage:
                        'assets/images/undraw_business_deal_cpi9.svg',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomSheet: const MobileCustomBottomSheet(),
    );
  }
}
