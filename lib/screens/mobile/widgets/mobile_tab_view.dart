import 'package:flutter/material.dart';

import 'sections/mobile_first_section.dart';
import 'sections/mobile_second_section.dart';
import 'sections/mobile_third_section.dart';
import 'sections/mobile_title_section.dart';

class MobileTabView extends StatelessWidget {
  final String title;
  final String firstSectionTitle;
  final String firstSectionImage;
  final String secondSectionTitle;
  final String secondSectionImage;
  final String thirdSectionTitle;
  final String thirdSectionImage;

  const MobileTabView({
    Key? key,
    required this.title,
    required this.firstSectionTitle,
    required this.firstSectionImage,
    required this.secondSectionTitle,
    required this.secondSectionImage,
    required this.thirdSectionTitle,
    required this.thirdSectionImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(230, 255, 250, 1),
            Color.fromRGBO(235, 244, 255, 1),
          ],
          begin: Alignment.topLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Column(
        children: [
          MobileTitleSection(title: title),
          MobileFirstSection(
            number: '1.',
            text: firstSectionTitle,
            image: firstSectionImage,
          ),
          MobileSecondSection(
            number: '2.',
            text: secondSectionTitle,
            image: secondSectionImage,
          ),
          MobileThirdSection(
            number: '3.',
            text: thirdSectionTitle,
            image: thirdSectionImage,
          ),
        ],
      ),
    );
  }
}
