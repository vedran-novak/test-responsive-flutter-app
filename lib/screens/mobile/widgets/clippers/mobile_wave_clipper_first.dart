import 'package:flutter/cupertino.dart';

class MobileFirstWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0.0, size.height - (size.height * 0.2));
    path.quadraticBezierTo(
      size.width / 6,
      size.height - (size.height * 0.25),
      size.width / 2,
      size.height - (size.height * 0.15),
    );

    path.quadraticBezierTo(
      size.width / 1.2,
      size.height - (size.height * 0.1),
      size.width,
      size.height - (size.height * 0.2),
    );
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
