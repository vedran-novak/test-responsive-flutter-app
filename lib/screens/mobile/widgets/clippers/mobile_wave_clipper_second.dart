import 'package:flutter/cupertino.dart';

class MobileSecondWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0, 0);
    path.cubicTo(
      size.width * 1 / 2,
      size.height,
      size.width * 2 / 4,
      0,
      size.width,
      size.height,
    );

    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
