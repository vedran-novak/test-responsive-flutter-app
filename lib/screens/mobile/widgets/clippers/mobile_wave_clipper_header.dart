import 'package:flutter/cupertino.dart';

class MobileHeaderWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0.0, size.height - 35);
    path.quadraticBezierTo(
      size.width / 6,
      size.height - 20,
      size.width / 2.5,
      size.height - 60,
    );

    path.quadraticBezierTo(
      size.width / 1.5,
      size.height - 110,
      size.width,
      size.height - 80,
    );
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
