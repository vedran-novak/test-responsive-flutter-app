import 'package:flutter/material.dart';

class MobileCustomSegmentedButtons extends StatefulWidget {
  final TabController tabController;

  const MobileCustomSegmentedButtons({Key? key, required this.tabController})
      : super(key: key);

  @override
  State<MobileCustomSegmentedButtons> createState() =>
      _MobileCustomSegmentedButtonsState();
}

class _MobileCustomSegmentedButtonsState
    extends State<MobileCustomSegmentedButtons>
    with SingleTickerProviderStateMixin {
  final List<String> _tabs = ['Arbeitnehmer', 'Arbeitgeber', 'Temporärbüro'];
  late TabController _tabController;
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _tabController = widget.tabController
      ..addListener(() {
        setState(() {});
        final screenWidth = MediaQuery.of(context).size.width;
        final containerWidth = screenWidth / 2.5;
        const padding = 20.0;
        final scrollDistance = (containerWidth + (padding * 2)) / 3;

        var result = _tabController.index * scrollDistance;
        _scrollController.animateTo(
          result,
          duration: const Duration(milliseconds: 300),
          curve: Curves.linear,
        );
      });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: SingleChildScrollView(
          clipBehavior: Clip.none,
          controller: _scrollController,
          physics: const NeverScrollableScrollPhysics(),
          scrollDirection: Axis.horizontal,
          child: DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              border: Border.all(
                color: const Color.fromRGBO(203, 213, 224, 1),
              ),
            ),
            child: TabBar(
              controller: _tabController,
              isScrollable: true,
              labelPadding: const EdgeInsets.all(0),
              indicator: BoxDecoration(
                color: const Color.fromRGBO(129, 230, 217, 1),
                borderRadius: BorderRadius.horizontal(
                  left: Radius.circular(_tabController.index == 0 ? 8.0 : 0.0),
                  right: Radius.circular(
                      _tabController.index == _tabs.length - 1 ? 8.0 : 0.0),
                ),
              ),
              dividerColor: Colors.transparent,
              indicatorColor: const Color.fromRGBO(129, 230, 217, 1),
              indicatorSize: TabBarIndicatorSize.tab,
              unselectedLabelColor: const Color.fromRGBO(49, 151, 149, 1),
              unselectedLabelStyle: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
              overlayColor:
                  MaterialStateProperty.all<Color>(Colors.transparent),
              labelColor: Colors.white,
              labelStyle: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
              tabs: [
                ..._tabs.map((tabName) => _individualTab(tabName)).toList(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _individualTab(String name) {
    return Container(
      clipBehavior: Clip.none,
      width: MediaQuery.of(context).size.width / 2.5,
      decoration: BoxDecoration(
        border: Border(
          right: name != 'Temporärbüro'
              ? const BorderSide(
                  color: Color.fromRGBO(203, 213, 224, 1),
                  style: BorderStyle.solid,
                )
              : BorderSide.none,
        ),
      ),
      child: Tab(
        text: name,
      ),
    );
  }
}
