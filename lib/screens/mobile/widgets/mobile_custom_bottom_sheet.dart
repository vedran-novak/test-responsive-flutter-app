import 'package:flutter/material.dart';
import 'package:test_mobile_index_vedran_novak/core/widgets/custom_register_button.dart';

class MobileCustomBottomSheet extends StatelessWidget {
  const MobileCustomBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: double.infinity,
        height: 88.0,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(255, 255, 255, 1),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.0),
            topRight: Radius.circular(12.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              blurRadius: 6,
              offset: Offset(0, -3), // changes position of shadow
            ),
          ],
        ),
        child: const CustomRegisterButton(
          addMargin: true,
        ),
      ),
    );
  }
}
