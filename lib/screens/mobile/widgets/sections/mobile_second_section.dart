import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:test_mobile_index_vedran_novak/screens/mobile/widgets/clippers/mobile_wave_clipper_second.dart';

import '../mobile_section_number_and_title.dart';

class MobileSecondSection extends StatelessWidget {
  final String number;
  final String text;
  final String image;

  const MobileSecondSection({
    Key? key,
    required this.number,
    required this.text,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 370.0,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
            top: -60,
            child: MobileSectionNumberAndTitle(
              number: number,
              title: text,
              color: Colors.transparent,
              isBaseline: true,
            ),
          ),
          Positioned(
            top: 80,
            left: 0,
            right: 0,
            bottom: 0,
            child: SvgPicture.asset(
              image,
              height: 144.55,
              width: double.maxFinite,
              clipBehavior: Clip.none,
              fit: BoxFit.none,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Transform.scale(
              scaleX: -1,
              child: SizedBox(
                height: 50.0,
                width: double.maxFinite,
                child: ClipPath(
                  clipper: MobileSecondWaveClipper(),
                  child: const DecoratedBox(
                    decoration: BoxDecoration(color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
