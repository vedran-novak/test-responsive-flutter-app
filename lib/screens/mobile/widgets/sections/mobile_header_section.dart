import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../clippers/mobile_wave_clipper_header.dart';

class MobileHeader extends StatelessWidget {
  const MobileHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 660.0 - kToolbarHeight,
      width: double.infinity,
      child: ClipPath(
        clipper: MobileHeaderWaveClipper(),
        child: DecoratedBox(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(235, 244, 255, 1),
                Color.fromRGBO(230, 255, 250, 1),
              ],
              begin: Alignment.topLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(
                  top: 18.0,
                  right: 20.0,
                  left: 20.0,
                  bottom: 20.0,
                ),
                child: SizedBox(
                  width: 350,
                  child: Text(
                    'Deine Job website',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 42.0,
                      letterSpacing: 1.26,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: SvgPicture.asset(
                  'assets/images/undraw_agreement_aajr.svg',
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
