import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../mobile_section_number_and_title.dart';

class MobileThirdSection extends StatelessWidget {
  final String number;
  final String text;
  final String image;

  const MobileThirdSection({
    Key? key,
    required this.number,
    required this.text,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400.0,
      color: Colors.white,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
            top: -60,
            child: MobileSectionNumberAndTitle(
              number: number,
              title: text,
              color: const Color.fromRGBO(247, 250, 252, 1),
              isBaseline: false,
            ),
          ),
          Positioned(
            top: 20,
            bottom: 0,
            right: 0,
            left: 50,
            child: SvgPicture.asset(
              image,
              height: 144.55,
              width: double.maxFinite,
              clipBehavior: Clip.none,
              fit: BoxFit.none,
            ),
          ),
        ],
      ),
    );
  }
}
