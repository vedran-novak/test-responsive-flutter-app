import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:test_mobile_index_vedran_novak/screens/mobile/widgets/clippers/mobile_wave_clipper_first.dart';

import '../mobile_section_number_and_title.dart';

class MobileFirstSection extends StatelessWidget {
  final String number;
  final String text;
  final String image;

  const MobileFirstSection({
    Key? key,
    required this.number,
    required this.text,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 370.0,
      child: ClipPath(
        clipper: MobileFirstWaveClipper(),
        child: DecoratedBox(
          decoration: const BoxDecoration(color: Colors.white),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                bottom: 40,
                child: MobileSectionNumberAndTitle(
                  number: number,
                  title: text,
                  color: const Color.fromRGBO(247, 250, 252, 1),
                  isBaseline: true,
                ),
              ),
              Positioned(
                top: -90,
                left: 60,
                right: 0,
                bottom: 0,
                child: SvgPicture.asset(
                  image,
                  height: 144.55,
                  width: double.maxFinite,
                  clipBehavior: Clip.none,
                  fit: BoxFit.none,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
