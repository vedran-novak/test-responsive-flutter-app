import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'sections/desktop_first_section.dart';
import 'sections/desktop_first_section_stacked.dart';
import 'sections/desktop_second_section.dart';
import 'sections/desktop_second_section_stacked.dart';
import 'sections/desktop_third_section.dart';
import 'sections/desktop_third_section_stacked.dart';
import 'sections/desktop_title_section.dart';

class DesktopTabView extends StatelessWidget {
  final String titleSection;
  final String firstSectionText;
  final String firstSectionImage;
  final String secondSectionText;
  final String secondSectionImage;
  final String thirdSectionText;
  final String thirdSectionImage;

  const DesktopTabView({
    Key? key,
    required this.titleSection,
    required this.firstSectionText,
    required this.firstSectionImage,
    required this.secondSectionText,
    required this.secondSectionImage,
    required this.thirdSectionText,
    required this.thirdSectionImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(230, 255, 250, 1),
            Color.fromRGBO(235, 244, 255, 1),
          ],
          begin: Alignment.topLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Stack(
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              DesktopTitleSection(title: titleSection),
              const Expanded(child: DesktopFirstSection()),
              const Expanded(child: DesktopSecondSection()),
              const Expanded(child: DesktopThirdSection()),
            ],
          ),
          DesktopThirdSectionStacked(
            number: '3.',
            text: thirdSectionText,
            image: thirdSectionImage,
          ),
          Positioned(
            left: MediaQuery.of(context).size.width * 0.25,
            top: MediaQuery.of(context).size.height * 0.84,
            width: MediaQuery.of(context).size.width * 0.34,
            height: MediaQuery.of(context).size.height * 0.4,
            child: SvgPicture.asset(
              'assets/images/gruppe_1822.svg',
              clipBehavior: Clip.none,
              fit: BoxFit.fill,
            ),
          ),
          DesktopSecondSectionStacked(
            number: '2.',
            text: secondSectionText,
            image: secondSectionImage,
          ),
          Positioned(
            left: MediaQuery.of(context).size.width * 0.25,
            top: MediaQuery.of(context).size.height * 0.37,
            width: MediaQuery.of(context).size.width * 0.34,
            height: MediaQuery.of(context).size.height * 0.41,
            child: SvgPicture.asset(
              'assets/images/gruppe_1821.svg',
              clipBehavior: Clip.none,
              fit: BoxFit.fill,
            ),
          ),
          DesktopFirstSectionStacked(
            number: '1.',
            text: firstSectionText,
            image: firstSectionImage,
          ),
        ],
      ),
    );
  }
}
