import 'package:flutter/material.dart';

class DesktopSectionNumberAndTitle extends StatelessWidget {
  final String number;
  final String title;
  final Color color;
  final bool isBaseline;

  const DesktopSectionNumberAndTitle({
    Key? key,
    required this.number,
    required this.title,
    required this.color,
    required this.isBaseline,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment:
          isBaseline ? CrossAxisAlignment.baseline : CrossAxisAlignment.center,
      textBaseline: TextBaseline.alphabetic,
      children: [
        Container(
          padding: const EdgeInsets.all(30.0),
          decoration: BoxDecoration(
            color: color,
            shape: BoxShape.circle,
          ),
          child: Text(
            number,
            style: const TextStyle(
              fontSize: 120,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(113, 128, 150, 1),
            ),
          ),
        ),
        const SizedBox(width: 10.0),
        Text(
          title,
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(113, 128, 150, 1),
            letterSpacing: 0.9,
          ),
        ),
      ],
    );
  }
}
