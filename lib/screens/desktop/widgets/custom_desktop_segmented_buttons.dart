import 'package:flutter/material.dart';

class CustomDesktopSegmentedButtons extends StatefulWidget {
  final TabController tabController;

  const CustomDesktopSegmentedButtons({Key? key, required this.tabController})
      : super(key: key);

  @override
  State<CustomDesktopSegmentedButtons> createState() =>
      _CustomDesktopSegmentedButtonsState();
}

class _CustomDesktopSegmentedButtonsState
    extends State<CustomDesktopSegmentedButtons> {
  final List<String> _tabs = ['Arbeitnehmer', 'Arbeitgeber', 'Temporärbüro'];
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = widget.tabController
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            border: Border.all(
              color: const Color.fromRGBO(203, 213, 224, 1),
            ),
          ),
          child: TabBar(
            controller: _tabController,
            isScrollable: true,
            labelPadding: const EdgeInsets.all(0),
            indicator: BoxDecoration(
              color: const Color.fromRGBO(129, 230, 217, 1),
              borderRadius: BorderRadius.horizontal(
                left: Radius.circular(_tabController.index == 0 ? 8.0 : 0.0),
                right: Radius.circular(
                    _tabController.index == _tabs.length - 1 ? 8.0 : 0.0),
              ),
            ),
            dividerColor: Colors.transparent,
            indicatorColor: const Color.fromRGBO(129, 230, 217, 1),
            indicatorSize: TabBarIndicatorSize.tab,
            unselectedLabelColor: const Color.fromRGBO(49, 151, 149, 1),
            unselectedLabelStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
            overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
            labelColor: Colors.white,
            labelStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
            tabs: [
              ..._tabs.map((tabName) => _individualTab(tabName)).toList(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _individualTab(String name) {
    return Container(
      clipBehavior: Clip.none,
      width: MediaQuery.of(context).size.width / 6,
      decoration: BoxDecoration(
        border: Border(
          right: name != 'Temporärbüro'
              ? const BorderSide(
                  color: Color.fromRGBO(203, 213, 224, 1),
                  style: BorderStyle.solid,
                )
              : BorderSide.none,
        ),
      ),
      child: Tab(
        text: name,
      ),
    );
  }
}
