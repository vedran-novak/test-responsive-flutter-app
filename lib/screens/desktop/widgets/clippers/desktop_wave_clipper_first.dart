import 'package:flutter/cupertino.dart';

class DesktopFirstWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0.0, size.height - (size.height * 0.12));
    path.quadraticBezierTo(
      size.width / 6,
      size.height - (size.height * 0.14),
      size.width / 3.5,
      size.height - (size.height * 0.1),
    );

    path.quadraticBezierTo(
      size.width / 2,
      size.height + (size.height * 0.05),
      size.width / 1.4,
      size.height - (size.height * 0.12),
    );

    path.quadraticBezierTo(
      size.width / 1.3,
      size.height - (size.height * 0.17),
      size.width / 0.7,
      size.height,
    );

    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
