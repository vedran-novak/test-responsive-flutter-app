import 'package:flutter/cupertino.dart';

class DesktopSecondWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0, size.height * 0.6);
    path.quadraticBezierTo(
      size.width / 6,
      size.height - (size.height * 0.2),
      size.width / 3.5,
      size.height - (size.height * 0.1),
    );

    path.quadraticBezierTo(
      size.width / 2,
      size.height + (size.height * 0.05),
      size.width / 1.4,
      size.height - (size.height * 0.2),
    );

    path.quadraticBezierTo(
      size.width / 1.3,
      size.height - (size.height * 0.31),
      size.width / 0.7,
      size.height,
    );

    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}

//import 'package:flutter/cupertino.dart';
//
// class SecondWaveClipper extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     var path = Path();
//
//     path.lineTo(0.0, size.height);
//     path.quadraticBezierTo(
//       size.width / 6,
//       size.height - 50,
//       size.width / 1.9,
//       size.height - 40,
//     );
//
//     path.quadraticBezierTo(
//       size.width / 1.2,
//       size.height - 30,
//       size.width,
//       size.height - 60,
//     );
//     path.lineTo(size.width, 0.0);
//     path.close();
//     return path;
//   }
//
//   @override
//   bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
//     return false;
//   }
// }
