import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:test_mobile_index_vedran_novak/core/widgets/custom_register_button.dart';

import '../clippers/desktop_header_wave_clipper.dart';

class DesktopHeader extends StatelessWidget {
  const DesktopHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 660.0 - kToolbarHeight,
      child: ClipPath(
        clipper: DesktopHeaderWaveClipper(),
        child: DecoratedBox(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(235, 244, 255, 1),
                Color.fromRGBO(230, 255, 250, 1),
              ],
              begin: Alignment.topLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 350,
                    child: Text(
                      'Deine Job website',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 65.0,
                        letterSpacing: 1.95,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 53,
                  ),
                  SizedBox(
                    width: 350,
                    child: CustomRegisterButton(addMargin: false),
                  ),
                ],
              ),
              Center(
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: ClipOval(
                    child: Container(
                      width: 400.0,
                      height: 400.0,
                      color: Colors.white,
                      child: SvgPicture.asset(
                        'assets/images/undraw_agreement_aajr.svg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
