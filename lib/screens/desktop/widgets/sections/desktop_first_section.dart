import 'package:flutter/material.dart';

import '../clippers/desktop_wave_clipper_first.dart';

class DesktopFirstSection extends StatelessWidget {
  const DesktopFirstSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: DesktopFirstWaveClipper(),
      child: DecoratedBox(
        decoration: const BoxDecoration(color: Colors.white),
        child: Container(),
      ),
    );
  }
}
