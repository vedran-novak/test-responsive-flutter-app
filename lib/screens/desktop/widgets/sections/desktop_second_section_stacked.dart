import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../desktop_section_number_and_title.dart';

class DesktopSecondSectionStacked extends StatelessWidget {
  final String number;
  final String text;
  final String image;

  const DesktopSecondSectionStacked({
    Key? key,
    required this.number,
    required this.text,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          left: MediaQuery.of(context).size.width * 0.55,
          top: MediaQuery.of(context).size.height * 0.75,
          child: DesktopSectionNumberAndTitle(
            number: number,
            title: text,
            color: const Color.fromRGBO(247, 250, 252, 1),
            isBaseline: true,
          ),
        ),
        Positioned(
          left: MediaQuery.of(context).size.width * 0.25,
          top: MediaQuery.of(context).size.height * 0.75,
          height: MediaQuery.of(context).size.height * 0.2,
          width: MediaQuery.of(context).size.width * 0.2,
          child: SvgPicture.asset(
            image,
            clipBehavior: Clip.none,
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }
}
