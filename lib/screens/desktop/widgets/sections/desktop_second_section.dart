import 'package:flutter/material.dart';

import '../clippers/desktop_wave_clipper_second.dart';

class DesktopSecondSection extends StatelessWidget {
  const DesktopSecondSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Transform.scale(
        scaleY: -1,
        child: SizedBox(
          height: 200.0,
          width: double.maxFinite,
          child: ClipPath(
            clipper: DesktopSecondWaveClipper(),
            child: const DecoratedBox(
              decoration: BoxDecoration(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
