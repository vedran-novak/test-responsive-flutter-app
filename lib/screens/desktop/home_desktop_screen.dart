import 'package:flutter/material.dart';
import 'package:test_mobile_index_vedran_novak/screens/desktop/widgets/desktop_tab_view.dart';

import '../../core/widgets/custom_app_bar.dart';
import 'widgets/custom_desktop_segmented_buttons.dart';
import 'widgets/sections/desktop_header.dart';

class HomeDesktopScreen extends StatefulWidget {
  const HomeDesktopScreen({super.key});

  @override
  State<HomeDesktopScreen> createState() => _HomeDesktopScreenState();
}

class _HomeDesktopScreenState extends State<HomeDesktopScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const DesktopHeader(),
            CustomDesktopSegmentedButtons(tabController: _tabController),
            SizedBox(
              height: MediaQuery.of(context).size.height * 1.8,
              child: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: const <Widget>[
                  DesktopTabView(
                    titleSection: 'Drei einfache Schritte zu deinem neuen Job',
                    firstSectionText: 'Erstellen dein Lebenslauf',
                    firstSectionImage:
                        'assets/images/undraw_Profile_data_re_v81r.svg',
                    secondSectionText: 'Erstellen dein Lebenslauf',
                    secondSectionImage: 'assets/images/undraw_task_31wc.svg',
                    thirdSectionText: 'Mit nur einem Klick bewerben',
                    thirdSectionImage:
                        'assets/images/undraw_personal_file_222m.svg',
                  ),
                  DesktopTabView(
                    titleSection:
                        'Drei einfache Schritte zu deinem neuen Mitarbeiter',
                    firstSectionText: 'Erstellen dein Unternehmensprofil',
                    firstSectionImage:
                        'assets/images/undraw_Profile_data_re_v81r.svg',
                    secondSectionText: 'Erstellen ein Jobinserat',
                    secondSectionImage:
                        'assets/images/undraw_about_me_wa29.svg',
                    thirdSectionText: 'Wähle deinen neuen Mitarbeiter aus',
                    thirdSectionImage:
                        'assets/images/undraw_swipe_profiles1_i6mr.svg',
                  ),
                  DesktopTabView(
                    titleSection:
                        'Drei einfache Schritte zu deinem neuen Mitarbeiter',
                    firstSectionText: 'Erstellen dein Unternehmensprofil',
                    firstSectionImage:
                        'assets/images/undraw_Profile_data_re_v81r.svg',
                    secondSectionText:
                        'Erhalte Vermittlungs- angebot von Arbeitgeber',
                    secondSectionImage:
                        'assets/images/undraw_job_offers_kw5d.svg',
                    thirdSectionText:
                        'Vermittlung nach Provision oder Stundenlohn',
                    thirdSectionImage:
                        'assets/images/undraw_business_deal_cpi9.svg',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
